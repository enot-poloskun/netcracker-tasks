/** @author Golovneva Maria
 *
 */
package ru.skillbench.tasks.text;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ContactCardImpl implements ContactCard{

    private String fn;
    private String org;
    private char gender;
    private Calendar bday;
    private ArrayList<String[]> tel;

    public ContactCardImpl(){  }

    @Override
    public ContactCard getInstance(Scanner scanner)  throws NoSuchElementException, InputMismatchException{

            scanner.useDelimiter("\r\n");
            String[] buf;
            String[] buf2;
            tel = new ArrayList<>();
            if (!(scanner.next().equals("BEGIN:VCARD"))) {
                throw new NoSuchElementException("First line must be BEGIN:VCARD");
            }
            String s = scanner.next();
            while(!s.equals("END:VCARD")){
                buf = s.split(":");
                if (buf.length < 2){
                    throw new InputMismatchException();
                }
                switch (buf[0]) {
                    case "FN":
                        fn = buf[1];
                        break;
                    case "ORG":
                        org = buf[1];
                        break;
                    case "GENDER":
                        if (buf[1].length()>1 || (buf[1].charAt(0)!='F' && buf[1].charAt(0)!='M'))
                        {
                            throw new InputMismatchException("Gender must be one character: F or M");
                        }
                        gender = buf[1].charAt(0);
                        break;
                    case "BDAY":
                        try {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                            calendar.setTime(format.parse(buf[1]));
                            bday = calendar;
                        } catch (Exception e) {
                            throw new InputMismatchException("Date must be in the following format DD-MM-YYYY");
                        }
                        break;
                    case "VERSION":
                    case "N":
                    case "TITLE":
                    case "EMAIL":
                    case "REV":
                    case "x-qq":
                        break;
                        default:
                            buf2 = buf[0].split(";");
                            switch (buf2[0]){
                                case "TEL":
                                    if (buf2[1].substring(0,5).equals("TYPE=")
                                            &&buf[1].matches("\\d{10}")) {
                                        tel.add(new String[]{buf2[1].substring(5), buf[1]});
                                    }
                                    else {
                                        throw new InputMismatchException("Wrong format for TEL");
                                    }
                                    break;
                                case "PHOTO":
                                case "ADR":
                                case "LABEL":
                                   break;
                                   default:
                                       throw new InputMismatchException("Unknown data");
                            }
                            break;

                }
                s = scanner.next();
            }
            if (fn == null || org == null)
                throw new NoSuchElementException("Required fields are missing");
            return this;
    }

    @Override
    public ContactCard getInstance(String data) {
        Scanner scanner = new Scanner(data);
        ContactCard card = getInstance(scanner);
        return card;
    }

    @Override
    public String getFullName() {
        return fn;
    }

    @Override
    public String getOrganization() {
        return org;
    }

    @Override
    public boolean isWoman() {
        if (gender == 'F')
            return true;
        else
            return false;
    }

    @Override
    public Calendar getBirthday() throws NoSuchElementException{
        if (bday == null)
            throw new NoSuchElementException();
        return bday;
    }

    @Override
    public Period getAge() throws NoSuchElementException{
        if (bday == null)
            throw new NoSuchElementException();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String start = format.format(bday.getTime());
        return Period.between(LocalDate.parse(start, formatter), LocalDate.now());
    }

    @Override
    public int getAgeYears() {
        if (bday == null)
            throw new NoSuchElementException();
        Period period = getAge();
        return period.getYears();
    }

    @Override
    public String getPhone(String type) throws NoSuchElementException{
        for (int i = 0; i < tel.size(); i++) {
           if (tel.get(i)[0].equals(type)){
               String part1 = tel.get(i)[1].substring(0,3);
               String part2 = tel.get(i)[1].substring(3,6);
               String part3 = tel.get(i)[1].substring(6);
               return "(" + part1 + ") " + part2 + "-" + part3;
           }
        }
        throw new NoSuchElementException();
    }

}
