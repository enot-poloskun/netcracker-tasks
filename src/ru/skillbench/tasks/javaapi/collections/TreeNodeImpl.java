/** @author Golovneva Maria
 *
 */
package ru.skillbench.tasks.javaapi.collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNodeImpl implements TreeNode {

    private TreeNode parent;
    private List<TreeNode> children;
    private boolean expanded;
    private Object data;

    @Override
    public TreeNode getParent() {
        return parent;
    }

    @Override
    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    @Override
    public TreeNode getRoot() {
        TreeNode root = this.parent;
        if (root == null)
            return null;
        while (root.getParent() != null) {
            root = root.getRoot();
        }
        return root;
    }

    @Override
    public boolean isLeaf() {
        if ((children == null) || (children.size() == 0))
            return true;
        else
            return false;
    }

    @Override
    public int getChildCount() {
        if (children == null)
            return 0;
        else
            return children.size();
    }

    @Override
    public Iterator<TreeNode> getChildrenIterator() {
        return children.iterator();
    }

    @Override
    public void addChild(TreeNode child) {
        if (children == null)
            children = new ArrayList<>();
        children.add(child);
        child.setParent(this);
    }

    @Override
    public boolean removeChild(TreeNode child) {
        boolean result = false;
        for (TreeNode node : children) {
            if (node.equals(child)) {
                result = children.remove(node);
                node.setParent(null);
                break;
            }
        }
        return result;
    }

    @Override
    public boolean isExpanded() {
        return expanded;
    }

    @Override
    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
        if (children != null)
            for (TreeNode node : children) {
                node.setExpanded(expanded);
            }
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String getTreePath() {
        String path;
        if (data == null)
            path = "empty";
        else
            path = data.toString();
        if (parent != null)
            path = parent.getTreePath() + "->" + path;
        return path;
    }

    @Override
    public TreeNode findParent(Object data) {
        TreeNode result;
        if (this.data == null && data == null)
            return this;
        else if (this.data.equals(data))
            return this;
        else if (parent == null)
            return null;
        else {
            result = parent.findParent(data);
        }
        return result;
    }

    @Override
    public TreeNode findChild(Object data) {
        TreeNode result = null;
       if (isLeaf())
            return null;
       else {
            for (TreeNode node : children) {
                if (node.getData() == null && data == null)
                    return node;
                else if ((node.getData()!=null)&&(node.getData().equals(data)))
                    return node;
                else {
                    result = node.findChild(data);
                    if ((result != null) && ((result.getData() != null && result.getData().equals(data)) ||
                                            (result.getData() == null && data == null))) {
                        break;
                    }

                }
            }

       }
        return result;
    }
    
}