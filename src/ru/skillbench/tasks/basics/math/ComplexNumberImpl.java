/** @author Golovneva Maria
 *
 */
package ru.skillbench.tasks.basics.math;

public class ComplexNumberImpl implements ComplexNumber {

    private double re;
    private double im;

    public ComplexNumberImpl(double re, double im) {
        this.re = re;
        this.im = im;
    }
    public ComplexNumberImpl(String value){
        this.set(value);
    }

    public ComplexNumberImpl(){

    }


    @Override
    public double getRe() {
        return re;
    }

    @Override
    public double getIm() {
        return im;
    }

    @Override
    public boolean isReal() {
        if (im == 0)
            return true;
        else
            return false;
    }

    @Override
    public void set(double re, double im) {
        this.re = re;
        this.im = im;
    }

    @Override
    public void set(String value) throws NumberFormatException {

        int i = Math.max(value.lastIndexOf("+"), value.lastIndexOf("-"));
        if (i == 0 || i == -1)
        {
            if  (value.endsWith("i"))
            {
                this.re = 0;
                if(value.equals("+i") || value.equals("i")) {
                    this.im = 1; }
                else if (value.equals("-i")) {
                    this.im = -1; }
                else {
                    this.im = Double.parseDouble(value.substring(0,value.length()-1)); }
            }
            else {
                this.re = Double.parseDouble(value);
                this.im = 0;
            }
        }
        else
        {
            String[] str = new String[2];
            str[0] = value.substring(0,i);
            str[1] = value.substring(i);
            this.re = Double.parseDouble(str[0]);
            if  (!str[1].endsWith("i")) {
                throw new NumberFormatException(); }
            if(str[1].equals("+i") || str[1].equals("i")) {
                this.im = 1; }
            else if (str[1].equals("-i")) {
                this.im = -1; }
            else {
                this.im = Double.parseDouble(str[1].substring(0,str[1].length()-1)); }
        }

    }

    @Override
    public ComplexNumber copy() {
        ComplexNumber copy = new ComplexNumberImpl();
        copy.set(this.re, this.im);
        return copy;
    }

    @Override
    public ComplexNumber clone() throws CloneNotSupportedException {
        return (ComplexNumber) super.clone();
    }

    @Override
    public String toString()
    {
        if (im == 0) {
            return Double.toString(re);
        }
        else if (re == 0) {
            return Double.toString(im)+"i";
        }
        else if (im > 0){
            return Double.toString(re) + "+" + Double.toString(im)+"i";
        }
        else {
            return Double.toString(re) + Double.toString(im)+"i";
        }
    }

    @Override
    public boolean equals(Object other){
        if (other == this) { return true; }
        if (other == null || !(other instanceof ComplexNumber)) { return false; }
        ComplexNumber comp = (ComplexNumber)other;

        return (this.im == comp.getIm()&&this.re == comp.getRe());

    }

    @Override
    public int compareTo(ComplexNumber other) {
        Double x = this.re*this.re + this.im*this.im;
        Double y = other.getRe()*other.getRe() + other.getIm()*other.getIm();
        return x.compareTo(y);
    }

    @Override
   public void sort(ComplexNumber[] array) {
        for(int i = array.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
                if( array[j].compareTo(array[j+1]) == 1 ) {
                    ComplexNumber tmp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tmp;
                }
            }
        }
    }

    @Override
    public ComplexNumber negate() {
        this.set(-1*re, -1*im);
        return this;
    }

    @Override
    public ComplexNumber add(ComplexNumber arg2) {
        this.set(re+arg2.getRe(), im+arg2.getIm());
        return this;
    }

    @Override
    public ComplexNumber multiply(ComplexNumber arg2) {
        this.set(re*arg2.getRe()-im*arg2.getIm(),im*arg2.getRe()+re*arg2.getIm());
        return this;
    }

}