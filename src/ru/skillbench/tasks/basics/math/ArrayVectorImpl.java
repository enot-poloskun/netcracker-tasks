/** @author Golovneva Maria
 *
 */
package ru.skillbench.tasks.basics.math;

import java.util.Arrays;

public class ArrayVectorImpl implements ArrayVector{
    private double[] elements;

    public ArrayVectorImpl() {
        elements = new double[]{1, 2, 3};
    }

    @Override
    public void set(double... elements) {

        this.elements = elements;

    }

    @Override
    public double[] get() {
        return elements;
    }

    @Override
    public ArrayVector clone() {
        ArrayVectorImpl result = new ArrayVectorImpl();
        result.elements = elements.clone();
        return result;
    }

    @Override
    public int getSize() {
        return elements.length;
    }

    @Override
    public void set(int index, double value) {
        if (index < 0) {
        }
        else if (index >= elements.length) {
            elements = Arrays.copyOf(elements, index+1);
            elements[index] = value;
        }
        else {
            elements[index] = value;
        }

    }

    @Override
    public double get(int index) throws ArrayIndexOutOfBoundsException {
        if (index < 0 || index >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("Array Index Out of Bounds");
        }
        else {
            return elements[index];
        }
    }

    @Override
    public double getMax() {
       double max = elements[0];
       for (int i = 1; i < elements.length; i++) {
           if (elements[i] > max)
               max = elements[i];
       }
        return max;
    }

    @Override
    public double getMin() {
        double min = elements[0];
        for (int i = 1; i < elements.length; i++) {
            if (elements[i] < min)
                min = elements[i];
        }
        return min;
    }

    @Override
    public void sortAscending() {
        for(int i = elements.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
            if( elements[j] > elements[j+1] ) {
                double tmp = elements[j];
                elements[j] = elements[j+1];
                elements[j+1] = tmp;
                }
            }
        }
    }

    @Override
    public void mult(double factor) {
        for (int i = 0; i < elements.length; i++)
        {
            elements[i] = elements[i] * factor;
        }
    }

    @Override
    public ArrayVector sum(ArrayVector anotherVector) {
        int n = Math.min(elements.length, anotherVector.getSize());
        for (int i = 0; i < n; i++){
           elements[i] = elements[i] + anotherVector.get(i);
        }
        return this;
    }

    @Override
    public double scalarMult(ArrayVector anotherVector) {
        double result = 0;
        int n = Math.min(elements.length, anotherVector.getSize());
        for (int i = 0; i < n; i++) {
            result += elements[i] * anotherVector.get(i);
        }
        return result;
    }

    @Override
    public double getNorm() {
        return Math.sqrt(this.scalarMult(this));
    }



    static public  void main(String[] args) {
        int i = 30;
        Integer X = i;
        byte b = 42;

        System.out.println(b);
    }

}
